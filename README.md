This is not README, but my task list

### Tasks ###

* ~~Database migration scripts (flyway or liquibase?);~~
* ~~Add unique compound constraint for user_roles (user_id, user_role), add unique constraint for email;~~
* ~~Add population of the database;~~
* ~~Add rollback when a liquibase script fails, add seq for database;~~
* ~~Registration form, but needs to be polished up a bit (clean the code and optimize it,~~ probabaly alerts are not good from UX point of view);
* ~~Login form;~~
* ~~Add auth token to header;~~
* Add log out;
* <b>User names should be unique</b>;
* UI interceptor for basic auth;
* ~~Create registration service for user;~~
* ~~Map registration form to real registrationService;~~
* ~~Navbar on the web (Home, tree, register etc.);~~
* Integrate tree component;
* ~~Correct path in spring security (make it possible to open the frontend);~~
* ~~Constraint for userName and userEmail;~~
* Make it possible to create tree nodes with admin rights;
* Create exception interceptor;
* Think about returning entities;
* Decide if rxJs dependencies are needed in package.json;
* ~~Define constants for REST APIs on UI~~;
* Add backend validator for entities;
* Add logger;
* Fix welcoming greeting;
* Indentation on login form;
* Disable reset button when no data is fiiled in;
* Empty user object (login and registration form are connected somehow);
* Add liquibase scripts for articles.