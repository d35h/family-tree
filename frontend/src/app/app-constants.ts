export class AppConstants {

  public static getRestURL(): string {
    return 'http://localhost:8080'
  }

  public static getRegisterUserEndPoint(): string {
    return '/open/register';
  }

  public static getLoginUserEndPoint(): string {
    return '/user/login'
  }

  public static getGetUserByIdEndPoint(): string {
    return '/rest/getUserById/'
  }

}
