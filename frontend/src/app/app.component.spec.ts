import {async, TestBed} from "@angular/core/testing";

import {AppComponent} from "./app.component";
import {NavbarComponent} from "./components/navbar/navbar.component";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";
import {AppRoutingModule} from "./app-routing.module";
import {HttpModule} from "@angular/http";
import {RegistrationFormComponent} from "./components/registration-form/registration-form.component";
import {HomeComponent} from "./components/home/home.component";
import {APP_BASE_HREF} from "@angular/common";
import {LoginComponent} from "./components/login/login.component";
import {TokenService} from "./services/token-service/token.service";

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        RegistrationFormComponent,
        HomeComponent,
        NavbarComponent,
        LoginComponent
      ],
      imports: [
        NgbModule.forRoot(),
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        HttpModule
      ],
      providers: [
        TokenService,
        { provide: APP_BASE_HREF, useValue : '/family-tree/' }
      ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

});
