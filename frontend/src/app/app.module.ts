import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {AppComponent} from './app.component';
import {RegistrationFormComponent} from './components/registration-form/registration-form.component';
import {AppRoutingModule} from "./app-routing.module";
import {HomeComponent} from "./components/home/home.component";
import {NavbarComponent} from "./components/navbar/navbar.component";
import {BackendService} from "./services/backend-service/backend.service";
import {User} from "./entities/user";
import {RegistrationService} from "./services/registration-service/registration.service";
import {LoginComponent} from "./components/login/login.component";
import {LoginService} from "./services/login-service/login.service";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {UserUtilsService} from "./services/user-utils-service/user-utils.service";
import {TokenService} from "./services/token-service/token.service";
import {AuthorizationInterceptor} from "./config/authorization.interceptor";
import {HttpClientTestingBackend} from "@angular/common/http/testing/src/backend";

@NgModule({
  declarations: [
    AppComponent,
    RegistrationFormComponent,
    HomeComponent,
    NavbarComponent,
    LoginComponent
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    BackendService,
    User,
    RegistrationService,
    LoginService,
    UserUtilsService,
    TokenService,
    {provide: HTTP_INTERCEPTORS, useClass: AuthorizationInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
