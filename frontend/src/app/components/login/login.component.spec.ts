import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {FormsModule} from "@angular/forms";

import {LoginComponent} from "./login.component";
import {User} from "../../entities/user";
import {LoginService} from "../../services/login-service/login.service";
import {BackendService} from "../../services/backend-service/backend.service";
import {HttpClientModule} from "@angular/common/http";
import {UserUtilsService} from "../../services/user-utils-service/user-utils.service";
import {TokenService} from "../../services/token-service/token.service";

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [FormsModule, HttpClientModule],
      providers: [User, LoginService, BackendService, UserUtilsService, TokenService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
