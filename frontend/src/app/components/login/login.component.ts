import {Component, OnInit} from "@angular/core";
import {User} from "../../entities/user";
import {LoginService} from "../../services/login-service/login.service";
import {TokenService} from "../../services/token-service/token.service";
import {BackendService} from "../../services/backend-service/backend.service";
import {AppConstants} from "../../app-constants";
import {HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private user: User, private loginService: LoginService, private tokenService: TokenService) {
  }

  ngOnInit() {
  }

  public isLoginButtonDisabled(): boolean {
    return !(this.user.userName && this.user.userPassword);
  }

  public loginUser(user: User): void {
    console.log('Login user', user);
    this.loginService.loginUser(user).subscribe(
      response => {
        this.successfulLogin(response);
      },
      err => {
        this.unsuccessfulLogin(err);
      }
    );
  }

  private successfulLogin(response): void {
    this.tokenService.setToken(response.body);
  }

  private unsuccessfulLogin(response): void {
    //TODO: Process unsuccessful login, probably throw an error,
    //TODO: which implies implementation of error interceptor on UI
    console.log('response', response);
  }
}
