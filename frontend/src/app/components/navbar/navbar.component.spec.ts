import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarComponent } from './navbar.component';
import { AppRoutingModule } from "../../app-routing.module";
import { RegistrationFormComponent } from "../registration-form/registration-form.component";
import { HomeComponent } from "../home/home.component";
import { FormsModule } from "@angular/forms";
import { APP_BASE_HREF } from "@angular/common";
import { LoginComponent } from "../login/login.component";
import {TokenService} from "../../services/token-service/token.service";

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        NavbarComponent,
        RegistrationFormComponent,
        HomeComponent,
        LoginComponent
      ],
      imports: [
        AppRoutingModule,
        FormsModule
      ],
      providers: [
        TokenService,
        {provide: APP_BASE_HREF, useValue: '/family-tree/'}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
