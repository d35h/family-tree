import { Component, OnInit } from '@angular/core';

import { RouterModule } from '@angular/router';
import {TokenService} from "../../services/token-service/token.service";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private tokenService: TokenService) { }

  ngOnInit() {
  }

  public isUserLoggedIn(): boolean {
    return this.tokenService.jwtToken != null;
  }

}
