import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {RegistrationFormComponent} from "./registration-form.component";
import {FormsModule} from "@angular/forms";
import {User} from "../../entities/user";
import {RegistrationService} from "../../services/registration-service/registration.service";
import {BackendService} from "../../services/backend-service/backend.service";
import {HttpModule} from "@angular/http";
import {HttpClientModule} from "@angular/common/http";
import {UserUtilsService} from "../../services/user-utils-service/user-utils.service";

describe('RegistrationFormComponent', () => {
  let component: RegistrationFormComponent;
  let fixture: ComponentFixture<RegistrationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RegistrationFormComponent],
      imports: [FormsModule, HttpModule, HttpClientModule],
      providers: [User, RegistrationService, BackendService, UserUtilsService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
