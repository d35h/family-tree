import { Component, OnInit } from '@angular/core';
import { User } from "../../entities/user";
import { NgForm } from "@angular/forms";
import { RegistrationService } from "../../services/registration-service/registration.service";
import { Observable } from "rxjs/Observable";

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css']
})
export class RegistrationFormComponent implements OnInit {

  constructor(private user: User, private registrationService: RegistrationService) {
  }

  ngOnInit() {
  }

  onSubmit(user: User, userForm: NgForm): void {
    console.log('Submitted user: ', user);
    this.registrationService.registerUser(user).subscribe();
    userForm.resetForm();
  }

}
