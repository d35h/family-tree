import {inject, TestBed} from "@angular/core/testing";

import {AuthorizationInterceptor} from "./authorization.interceptor";
import {TokenService} from "../services/token-service/token.service";
import {HttpClientModule} from "@angular/common/http";
import {BackendService} from "../services/backend-service/backend.service";

describe('AuthorizationInterceptor', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        AuthorizationInterceptor,
        BackendService,
        TokenService]
    });
  });

  it('should be created', inject([AuthorizationInterceptor], (service: AuthorizationInterceptor) => {
    expect(service).toBeTruthy();
  }));

});
