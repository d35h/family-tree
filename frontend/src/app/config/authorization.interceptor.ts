import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import {Observable} from "rxjs/Observable";
import {TokenService} from "../services/token-service/token.service";

@Injectable()
export class AuthorizationInterceptor implements HttpInterceptor {
  constructor(private tokenService: TokenService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.tokenService.jwtToken != null) {
      const authReq = req.clone({setHeaders: {Authorization: this.tokenService.jwtToken.toString()}});
      return next.handle(authReq);
    }
    return next.handle(req);
  }
}
