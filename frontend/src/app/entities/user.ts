import { BaseEntity } from "./base-entity";
import { Injectable } from "@angular/core";

@Injectable()
export class User extends BaseEntity {
  userName: string;
  userPassword: string;
}
