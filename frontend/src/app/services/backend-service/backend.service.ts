import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";

import "rxjs/add/operator/map";
import {AppConstants} from "../../app-constants";
import {HttpClient} from "@angular/common/http";
/**
 * This service is used to carry out the http requests
 */
@Injectable()
export class BackendService {

  constructor(private http: HttpClient) {}

  public executeGetRequest(url: String, body?: any, options?: any): Observable<any> {
    return this.http.get(AppConstants.getRestURL() + url + body, options);
  }

  public executePostRequest(url: String, body: any, options?: any): Observable<any> {
    return this.http.post(AppConstants.getRestURL() + url, body, options);
  }

}
