import {inject, TestBed} from "@angular/core/testing";

import {LoginService} from "./login.service";
import {BackendService} from "../backend-service/backend.service";
import {HttpClientModule} from "@angular/common/http";
import {User} from "../../entities/user";
import {UserUtilsService} from "../user-utils-service/user-utils.service";
import {Observable} from "rxjs/Observable";
import {AppConstants} from "../../app-constants";

describe('LoginService', () => {
  let spy: any;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [HttpClientModule],
      providers: [LoginService, BackendService, UserUtilsService]
    });
  });

  it('should be created', inject([LoginService], (service: LoginService) => {
    expect(service).toBeTruthy();
  }));

  it('should not sent a request when user is null', inject([LoginService, BackendService], (loginService: LoginService, backendService: BackendService) => {
    let user = new User();
    spy = spyOn(backendService, 'executePostRequest');

    loginService.loginUser(user);
    expect(backendService.executePostRequest).not.toHaveBeenCalled();
  }));

  it('should sent a request when user is filled', inject([LoginService, BackendService], (loginService: LoginService, backendService: BackendService) => {
    let jwtToken: String = "jWtToKeN7hBdWcaJNjl";
    let result;
    let user = new User();
    user.userPassword = "userPassword";
    user.userName = "userName";

    spy = spyOn(backendService, 'executePostRequest').and.returnValue(Observable.of(jwtToken));

    loginService.loginUser(user).subscribe((response: Response) => { result = response });
    expect(backendService.executePostRequest).not.toHaveBeenCalledWith(AppConstants.getLoginUserEndPoint, user);
    expect(result).toBe(jwtToken);
  }));
});
