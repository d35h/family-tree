import {Injectable} from '@angular/core';
import {BackendService} from "../backend-service/backend.service";
import {User} from "../../entities/user";
import {Observable} from "rxjs/Observable";
import {AppConstants} from "../../app-constants";
import {HttpHeaders} from "@angular/common/http";
import {UserUtilsService} from "../user-utils-service/user-utils.service";

@Injectable()
export class LoginService {

  constructor(private backendService: BackendService, private userUtils: UserUtilsService) {
  }

  public loginUser(user: User): Observable<any> {
    if (!this.userUtils.isUserFilled(user)) {
      return;
    }

    let headers = new HttpHeaders()
      .set("Access-Control-Allow-Origin", "*")
      .set("Content-Type", "application/json");
    return this.backendService.executePostRequest(AppConstants.getLoginUserEndPoint(), user, headers);
  }

}
