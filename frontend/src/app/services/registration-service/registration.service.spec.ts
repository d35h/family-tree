import {inject, TestBed} from "@angular/core/testing";

import {RegistrationService} from "./registration.service";
import {BackendService} from "../backend-service/backend.service";
import {User} from "../../entities/user";
import {Observable} from "rxjs/Observable";
import {AppConstants} from "../../app-constants";
import "rxjs/add/observable/of";
import {HttpClientModule} from "@angular/common/http";
import {UserUtilsService} from "../user-utils-service/user-utils.service";

describe('RegistrationService', () => {
  let spy: any;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [RegistrationService, BackendService, UserUtilsService]
    });
  });

  it('should be created', inject([RegistrationService], (service: RegistrationService) => {
    expect(service).toBeTruthy();
  }));

  it('should not call backend API when user is null', inject([RegistrationService, BackendService], (registrationService: RegistrationService, backendService: BackendService) => {
    let user = new User();
    spy = spyOn(backendService, 'executePostRequest');

    registrationService.registerUser(user);
    expect(backendService.executePostRequest).not.toHaveBeenCalled();
  }));

  it('should not call backend API when userName is null', inject([RegistrationService, BackendService], (registrationService: RegistrationService, backendService: BackendService) => {
    let user = new User();
    user.userPassword = "userPassword";
    spy = spyOn(backendService, 'executePostRequest');

    registrationService.registerUser(user);
    expect(backendService.executePostRequest).not.toHaveBeenCalled();
  }));

  it('should not call backend API when userName is empty', inject([RegistrationService, BackendService], (registrationService: RegistrationService, backendService: BackendService) => {
    let user = new User();
    user.userName = "";
    user.userPassword = "userPassword";
    spy = spyOn(backendService, 'executePostRequest');

    registrationService.registerUser(user);
    expect(backendService.executePostRequest).not.toHaveBeenCalled();
  }));

  it('should not call backend API when userPassword is null', inject([RegistrationService, BackendService], (registrationService: RegistrationService, backendService: BackendService) => {
    let user = new User();
    user.userName = "userName";
    spy = spyOn(backendService, 'executePostRequest');

    registrationService.registerUser(user);
    expect(backendService.executePostRequest).not.toHaveBeenCalled();
  }));

  it('should not call backend API when userPassword is empty', inject([RegistrationService, BackendService], (registrationService: RegistrationService, backendService: BackendService) => {
    let user = new User();
    user.userName = "userName";
    user.userPassword = "";
    spy = spyOn(backendService, 'executePostRequest');

    registrationService.registerUser(user);
    expect(backendService.executePostRequest).not.toHaveBeenCalled();
  }));

  it('should register user', inject([RegistrationService, BackendService], (registrationService: RegistrationService, backendService: BackendService) => {
    let user = new User();
    user.userName = "userName";
    user.userPassword = "userPassword";
    spy = spyOn(backendService, 'executePostRequest').and.returnValue(Observable.of(user));

    let result: User;
    registrationService.registerUser(user).subscribe(response => result = response);
    user.id = 1;
    expect(backendService.executePostRequest).toHaveBeenCalledWith(AppConstants.getRegisterUserEndPoint(), user);
    expect(result.id).toBe(user.id);
    expect(result.userName).toBe(user.userName);
    expect(result.userPassword).toBe(user.userPassword);
  }));
});
