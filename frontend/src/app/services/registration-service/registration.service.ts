import { Injectable } from '@angular/core';
import {BackendService} from "../backend-service/backend.service";
import {User} from "../../entities/user";
import {Observable} from "rxjs/Observable";
import {AppConstants} from "../../app-constants";
import {UserUtilsService} from "../user-utils-service/user-utils.service";

@Injectable()
export class RegistrationService {

  constructor(private backendService: BackendService, private userUtils: UserUtilsService) {}

  public registerUser(user: User): Observable<User> {
    if (!this.userUtils.isUserFilled(user)) {
      console.log('Unable to save user, user is not filled with values');
      return;
    }

    return this.backendService.executePostRequest(AppConstants.getRegisterUserEndPoint(), user);
  }
}
