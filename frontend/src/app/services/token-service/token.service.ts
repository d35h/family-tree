import { Injectable } from '@angular/core';

@Injectable()
export class TokenService {
  jwtToken: String;

  constructor() { }

  public setToken(jwtToken: String): void {
    this.jwtToken = jwtToken;
  }

}
