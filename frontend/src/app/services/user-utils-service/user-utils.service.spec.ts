import { TestBed, inject } from '@angular/core/testing';

import { UserUtilsService } from './user-utils.service';
import {User} from "../../entities/user";

describe('UserUtilsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserUtilsService]
    });
  });

  it('should be created', inject([UserUtilsService], (service: UserUtilsService) => {
    expect(service).toBeTruthy();
  }));

  it('should return false when user is null', inject([UserUtilsService], (service: UserUtilsService) => {
    let user: User;
    expect(service.isUserFilled(user)).toBeFalsy();
  }));

  it('should return false when only userName is null and userPassword is filled', inject([UserUtilsService], (service: UserUtilsService) => {
    let user = new User();
    user.userPassword = "userPassword";
    expect(service.isUserFilled(user)).toBeFalsy();
  }));

  it('should return false when only userName is empty and userPassword is filled', inject([UserUtilsService], (service: UserUtilsService) => {
    let user = new User();
    user.userName = "";
    user.userPassword = "userPassword";
    expect(service.isUserFilled(user)).toBeFalsy();
  }));

  it('should return false when userName is filled and userPassword is null', inject([UserUtilsService], (service: UserUtilsService) => {
    let user = new User();
    user.userName = "userName";
    expect(service.isUserFilled(user)).toBeFalsy();
  }));

  it('should return false when userName is filled and userPassword is empty', inject([UserUtilsService], (service: UserUtilsService) => {
    let user = new User();
    user.userName = "userName";
    user.userPassword = "";
    expect(service.isUserFilled(user)).toBeFalsy();
  }));

  it('should return false when user is not null and fields are filled', inject([UserUtilsService], (service: UserUtilsService) => {
    let user = new User();
    user.userName = "userName";
    user.userPassword = "userPassword";
    expect(service.isUserFilled(user)).toBeTruthy();
  }));
});
