import {Injectable} from "@angular/core";
import {User} from "../../entities/user";

@Injectable()
export class UserUtilsService {

  constructor() { }

  public isUserFilled(user: User): boolean {
    return user != null && user.userPassword != null && user.userPassword != "" && user.userName != null && user.userName != "";
  }

}
