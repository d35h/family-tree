package org.familytree.familytree.config.filters;

import java.io.IOException;
import java.util.stream.Collectors;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jdk.nashorn.internal.parser.Token;
import org.familytree.familytree.entities.User;
import org.familytree.familytree.services.TokenAuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * JWT login filter.
 */
public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {

    private static final Logger logger = LoggerFactory.getLogger(JWTLoginFilter.class);

    public JWTLoginFilter(String url, AuthenticationManager authManager) {
        super(new AntPathRequestMatcher(url));
        setAuthenticationManager(authManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) throws AuthenticationException, IOException, ServletException {
        User user = new ObjectMapper().readValue(req.getInputStream(), User.class);

        logger.debug("The following user tries to log in: {}", user);

        return getAuthenticationManager().authenticate(
                new UsernamePasswordAuthenticationToken(
                        user.getUserName(),
                        user.getUserPassword(),
                        user.getUserRolesOrDefault()
                                .stream().map(role -> new SimpleGrantedAuthority(role.getRoleName()))
                                .collect(Collectors.toList())
                )
        );
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res, FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {
        logger.info("User with the name {} has successfully logged in", auth.getName());
        String jwtToken = TokenAuthenticationService.generateJWT(auth, System.currentTimeMillis());
        TokenAuthenticationService.addAuthentication(res, jwtToken);
        writeJWTTokenToResponse(res, jwtToken);
    }

    private void writeJWTTokenToResponse(HttpServletResponse response, String jwtToken) throws IOException {
        ResponseEntity<String> responseEntity = new ResponseEntity<>(jwtToken, HttpStatus.OK);
        response.getWriter().write(new ObjectMapper().writeValueAsString(responseEntity));
    }
}