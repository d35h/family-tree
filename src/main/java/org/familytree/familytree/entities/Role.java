package org.familytree.familytree.entities;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.familytree.familytree.enums.Roles;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "FT_Role")
public class Role implements Serializable {

    @Id
    @Column(name = "role_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="role_sequence")
    @SequenceGenerator(name = "role_sequence", sequenceName = "role_sequence", allocationSize = 1)
    private long roleId;

    @Column(name = "role_name", nullable = false, length = 50)
    private String roleName;

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Role() {
        super();
    }

    public Role(long roleId, String roleName) {
        super();

        this.roleId = roleId;
        this.roleName = (roleName == null || roleName.isEmpty() ? Roles.USER.getRole() : roleName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;

        return new EqualsBuilder()
                .append(roleId, role.roleId)
                .append(roleName, role.roleName)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(roleId)
                .append(roleName)
                .toHashCode();
    }
}
