package org.familytree.familytree.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "FT_User")
public class User implements Serializable {

    @Id
    @Column(name = "user_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="user_sequence")
    @SequenceGenerator(name = "user_sequence", sequenceName = "user_sequence", allocationSize = 1)
    private long userId;

    @Column(name = "user_name", nullable = false, length = 50)
    private String userName;

    @Column(name = "user_password", nullable = false, length = 50)
    private String userPassword;

    @Column(name = "user_email", nullable = false, length = 50)
    private String userEmail;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "FT_User_roles", joinColumns = @JoinColumn(name = "user_roles_user_id",
            referencedColumnName = "user_id"),
            inverseJoinColumns = @JoinColumn(
            name = "user_roles_role_id", referencedColumnName = "role_id"))
    private List<Role> userRoles;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public List<Role> getUserRolesOrDefault() {
        return userRoles == null ? (userRoles = new ArrayList<>()) : userRoles;
    }

    public void setUserRoles(List<Role> userRoles) {
        this.userRoles = userRoles;
    }

    public User() {
        super();
    }

    public User(Builder userBuilder) {
        this.userName = userBuilder.userName;
        this.userId = userBuilder.userId;
        this.userEmail = userBuilder.userEmail;
        this.userRoles = userBuilder.userRoles;
        this.userPassword = userBuilder.userPassword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return new org.apache.commons.lang3.builder.EqualsBuilder()
                .append(userId, user.userId)
                .append(userName, user.userName)
                .append(userPassword, user.userPassword)
                .append(userEmail, user.userEmail)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new org.apache.commons.lang3.builder.HashCodeBuilder(17, 37)
                .append(userId)
                .append(userName)
                .append(userPassword)
                .append(userEmail)
                .toHashCode();
    }

    public static class Builder {
        private long userId;
        private String userName;
        private String userPassword;
        private String userEmail;
        private List<Role> userRoles;

        public Builder userId(long userId) {
            this.userId = userId;
            return this;
        }

        public Builder userName(String userName) {
            this.userName = userName;
            return this;
        }

        public Builder userPassword(String userPassword) {
            this.userPassword = userPassword;
            return this;
        }

        public Builder userEmail(String userEmail) {
            this.userEmail = userEmail;
            return this;
        }

        public Builder userRoles(List<Role> userRoles) {
            this.userRoles = userRoles;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }
}
