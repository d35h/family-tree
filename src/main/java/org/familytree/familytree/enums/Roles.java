package org.familytree.familytree.enums;

/**
 * This enum represents roles related to user.
 */
public enum Roles {
    USER("User"), ADMIN("Admin");

    private String role;

    Roles(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
