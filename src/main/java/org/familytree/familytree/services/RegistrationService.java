package org.familytree.familytree.services;

import org.familytree.familytree.entities.User;

/**
 * Registration service for registration of a new user.
 */
public interface RegistrationService {

    /**
     * Registers a new user to the database.
     *
     * @param user a user to register.
     * @return id of saved user.
     */
    Long registerUser(User user);

}
