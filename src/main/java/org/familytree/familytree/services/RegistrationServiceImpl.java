package org.familytree.familytree.services;

import org.familytree.familytree.entities.User;
import org.familytree.familytree.enums.Roles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Override
    public Long registerUser(User user) {
         /* Automatically populates user with 'user' role when no roles in user entity are specified */
        if (user.getUserRolesOrDefault().isEmpty()) {
            user.getUserRolesOrDefault().add(roleService.getRoleByName(Roles.USER.getRole()));
        }

        return userService.saveUser(user);
    }
}
