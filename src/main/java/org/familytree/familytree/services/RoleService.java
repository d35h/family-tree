package org.familytree.familytree.services;

import org.familytree.familytree.entities.Role;

/**
 * Role service for obtaining/saving of role class.
 */
public interface RoleService {

    /**
     * Returns role entity by a specified role name.
     *
     * @param roleName a role name to look by.
     * @return roles which was found by a specified role name.
     */
    Role getRoleByName(String roleName);

}
