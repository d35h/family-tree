package org.familytree.familytree.services;

import org.familytree.familytree.entities.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    RoleRepository roleRepository;

    @Override
    public Role getRoleByName(String roleName) {
        if (roleName == null || roleName.isEmpty()) {
            return null;
        }

        return roleRepository.findByRoleNameIgnoreCase(roleName);
    }
}
