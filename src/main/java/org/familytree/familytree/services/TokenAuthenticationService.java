package org.familytree.familytree.services;

import static java.util.Arrays.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * Service which deals with JTW tokens.
 */
@SuppressWarnings("unchecked")
public class TokenAuthenticationService {

    private static final Logger logger = LoggerFactory.getLogger(TokenAuthenticationService.class);

    private static final long EXPIRATION_TIME = 864_000_000; // 10 days

    private static final String SECRET = "ThisIsASecret";

    private static final String HEADER_AUTHORIZATION = "Authorization";

    private static final String CLAIMS_AUTHORITIES_KEY = "authorities";

    private static final String CLAIMS_CREDENTIALS_KEY = "credentials";

    private static final String CLAIMS_SEPARATOR = ",";

    public static void addAuthentication(HttpServletResponse res, String jwtToken) {
        res.addHeader(HEADER_AUTHORIZATION, jwtToken);
    }

    public static String generateJWT(Authentication auth, long currentTimeMillis) {
        Claims claims = getClaims(auth);

        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(new Date(currentTimeMillis + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
    }


    public static Authentication getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_AUTHORIZATION);
        List<SimpleGrantedAuthority> authorities;

        if (token != null) {
            Claims body = Jwts.parser()
                    .setSigningKey(SECRET)
                    .parseClaimsJws(token)
                    .getBody();

            authorities = getSimpleGrantedAuthoritiesFromClaims(body);

            return new UsernamePasswordAuthenticationToken(body.getSubject(), body.get(CLAIMS_CREDENTIALS_KEY), authorities);
        }

        return null;
    }

    private static Claims getClaims(Authentication auth) {
        Claims claims = Jwts.claims().setSubject(auth.getName());
        claims.put(CLAIMS_AUTHORITIES_KEY, auth.getAuthorities().stream().map(authority -> authority.getAuthority()).collect(Collectors.joining(CLAIMS_SEPARATOR)));
        claims.put(CLAIMS_CREDENTIALS_KEY, auth.getCredentials());
        return claims;
    }

    private static List<SimpleGrantedAuthority> getSimpleGrantedAuthoritiesFromClaims(Claims claimsBody) {
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        try {
            String authoritiesString = (String) claimsBody.get(CLAIMS_AUTHORITIES_KEY);
            authorities = asList(authoritiesString.split(CLAIMS_SEPARATOR)).stream().map(authority -> new SimpleGrantedAuthority(authority))
                    .collect(Collectors.toList());
        } catch (JwtException | ClassCastException e) {
            logger.info("Failed during the casting of authorities: {}", e);
        }
        return authorities;
    }
}
