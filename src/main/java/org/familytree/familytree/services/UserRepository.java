package org.familytree.familytree.services;

import org.familytree.familytree.entities.User;
import org.springframework.data.repository.Repository;

/**
 * User repository which communicates with the database.
 * Mostly auto-generated by hibernate queries are used.
 */
public interface UserRepository extends Repository<User, Long> {

    /**
     * Looks for user entity by a specified user id.
     *
     * @param id an user ID look by.
     * @return user which was found by a specified ID.
     */
    User findByUserId(long id);

    /**
     * Looks for user entity by a specified user name, case sensitive.
     *
     * @param userName a role name to look by.
     * @return user which was found by a specified role name.
     */
    User findByUserName(String userName);

    /**
     * Persists a user entity to the database and returns saved entity.
     *
     * @param user a user to persist.
     * @return persisted user.
     */
    User save(User user);

}
