package org.familytree.familytree.services;

import org.familytree.familytree.entities.User;

/**
 * User service for obtaining/saving of user class.
 */
public interface UserService {

    /**
     * Looks for user entity by a specified user id.
     *
     * @param id an user ID look by.
     * @return user which was found by a specified ID.
     */
    User getUserById(long userId);

    /**
     * Looks for user entity by a specified user name, case sensitive.
     *
     * @param userName a role name to look by.
     * @return user which was found by a specified role name.
     */
    User getUserByName(String userName);

    /**
     * Persists a user entity to the database and returns ID of saved entity.
     *
     * @param user a user to persist.
     * @return ID of persisted user.
     */
    Long saveUser(User user);
}
