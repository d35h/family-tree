package org.familytree.familytree.services;

import org.familytree.familytree.entities.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    public User getUserById(long userId) {
        return userRepository.findByUserId(userId);
    }

    @Override
    public User getUserByName(String userName) {
        if (userName == null || userName.isEmpty()) {
            logger.info("Unable to get user by name, since name is not specified");
            return null;
        }

        logger.info("Looking for user with the name {}", userName);
        return userRepository.findByUserName(userName);
    }

    @Override
    public Long saveUser(User user) {
        if (user == null) {
            logger.info("Unable to save user, user object is null");
            return null;
        }

        logger.info("Saving user with the name {}", user.getUserName());
        return userRepository.save(user).getUserId();
    }
}
