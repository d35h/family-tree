package org.familytree.familytree.web;

import org.familytree.familytree.entities.User;
import org.familytree.familytree.services.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Registration REST controller.
 */
@RestController
public class RegistrationController {

    @Autowired
    private RegistrationService registrationService;

    @PostMapping("/open/register")
    public ResponseEntity<Long> registerUser(@RequestBody User user) {
        System.out.println("Registration of user: " + user);
        return ResponseEntity.ok(registrationService.registerUser(user));
    }

}
