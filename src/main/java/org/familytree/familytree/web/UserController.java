package org.familytree.familytree.web;

import org.familytree.familytree.entities.User;
import org.familytree.familytree.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * User REST controller.
 */
@RestController
public class UserController {

    private static Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @GetMapping("/rest/getUserById/{userId}")
    public ResponseEntity<User> getUserById(@PathVariable("userId") Long userId) {
        logger.info("Getting user with id: " + userId);
        return ResponseEntity.ok(userService.getUserById(userId));
    }

    @PostMapping("/rest/saveUser")
    public ResponseEntity<Long> saveUser(@RequestBody User user) {
        logger.info("Saving user with id: " + user.getUserId());
        return ResponseEntity.ok(userService.saveUser(user));
    }
}
