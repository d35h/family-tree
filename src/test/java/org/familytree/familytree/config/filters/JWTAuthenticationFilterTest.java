package org.familytree.familytree.config.filters;

import static org.hamcrest.core.Is.*;
import static org.junit.Assert.*;
import static org.unitils.reflectionassert.ReflectionAssert.*;
import static test.utilities.JWTUtility.*;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.context.SecurityContextHolder;

@RunWith(BlockJUnit4ClassRunner.class)
public class JWTAuthenticationFilterTest {

    @Test
    public void shouldSetAuthenticationSuccessfully() throws IOException, ServletException {
        JWTAuthenticationFilter filter = new JWTAuthenticationFilter();
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(HEADER_AUTHORIZATION, DEFAULT_JWT_STRING);

        HttpServletResponse response = new MockHttpServletResponse();
        FilterChain filterChain = new MockFilterChain();

        filter.doFilter(request, response, filterChain);
        assertThat(SecurityContextHolder.getContext().getAuthentication(), is(DEFAULT_AUTHENTICATION));
        assertThat(SecurityContextHolder.getContext().getAuthentication().getCredentials(), is(USER_PASSWORD));
        assertThat(SecurityContextHolder.getContext().getAuthentication().getName(), is(USER_NAME));
        assertReflectionEquals(SecurityContextHolder.getContext().getAuthentication().getAuthorities(), USER_AUTHORITIES);
    }

}
