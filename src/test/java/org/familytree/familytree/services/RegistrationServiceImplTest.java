package org.familytree.familytree.services;

import org.familytree.familytree.entities.Role;
import org.familytree.familytree.entities.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RegistrationServiceImplTest {

    @Mock
    UserServiceImpl userService;

    @Mock
    RoleServiceImpl roleService;

    @InjectMocks
    RegistrationServiceImpl registrationService;

    private final static User USER = new User.Builder()
            .userEmail("email")
            .userId(1)
            .userPassword("password")
            .userName("name")
            .build();
    private final static String USER_ROLE_NAME = "User";
    private final static Role ROLE = new Role(1L, USER_ROLE_NAME);

    @Before
    public void setUp() {
        when(userService.saveUser(USER)).thenReturn(USER.getUserId());
        when(roleService.getRoleByName(USER_ROLE_NAME)).thenReturn(ROLE);
    }

    @Test
    public void shouldRegisterUserAndPopulateRoles() {
        USER.setUserRoles(null);
        assertThat(registrationService.registerUser(USER), is(USER.getUserId()));
        assertThat(USER.getUserRolesOrDefault(), is(asList(ROLE)));

        verify(userService).saveUser(USER);
        verify(roleService).getRoleByName(USER_ROLE_NAME);
        verifyNoMoreInteractions(userService, roleService);
    }

    @Test
    public void shouldRegisterUserAndNotPopulateRoles() {
        USER.setUserRoles(asList(ROLE));
        assertThat(registrationService.registerUser(USER), is(USER.getUserId()));

        verify(userService).saveUser(USER);
        verifyZeroInteractions(roleService);
        verifyNoMoreInteractions(userService);
    }

}
