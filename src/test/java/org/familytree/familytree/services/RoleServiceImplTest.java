package org.familytree.familytree.services;

import org.familytree.familytree.entities.Role;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RoleServiceImplTest {

    @Mock
    RoleRepository roleRepository;

    @InjectMocks
    RoleServiceImpl roleService;

    private final static String USER_ROLE_NAME = "USER";
    private final static Role ROLE = new Role(1L, "USER");

    @Before
    public void setUp() {
        when(roleRepository.findByRoleNameIgnoreCase(USER_ROLE_NAME)).thenReturn(ROLE);

    }

    @Test
    public void shouldReturnRoleName() {
        assertThat(roleService.getRoleByName(USER_ROLE_NAME), is(ROLE));

        verify(roleRepository).findByRoleNameIgnoreCase(USER_ROLE_NAME);
        verifyNoMoreInteractions(roleRepository);
    }

    @Test
    public void shouldReturnReturnNullWhenNullIsAssigned() {
        assertNull(roleService.getRoleByName(null));

        verifyZeroInteractions(roleRepository);
    }

    @Test
    public void shouldReturnReturnNullWhenEmptyStringIsAssigned() {
        assertNull(roleService.getRoleByName(""));

        verifyZeroInteractions(roleRepository);
    }

}
