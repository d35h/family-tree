package org.familytree.familytree.services;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.unitils.reflectionassert.ReflectionAssert.*;
import static test.utilities.JWTUtility.*;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

@RunWith(BlockJUnit4ClassRunner.class)
public class TokenAuthenticationServiceTest {

    @Test
    public void shouldAddAuthenticationToResponse() {
        HttpServletResponse actual = new MockHttpServletResponse();
        String JWT = DEFAULT_JWT_STRING;

        TokenAuthenticationService.addAuthentication(actual, JWT);
        assertTrue(actual.containsHeader(HEADER_AUTHORIZATION));
        assertThat(actual.getHeader(HEADER_AUTHORIZATION), is(JWT));
    }

    @Test
    public void shouldGetAuthenticationFromRequest() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(HEADER_AUTHORIZATION, DEFAULT_JWT_STRING);

        Authentication actual = TokenAuthenticationService.getAuthentication(request);
        List<SimpleGrantedAuthority> authorities = (List<SimpleGrantedAuthority>) actual.getAuthorities();
        assertThat(authorities, hasSize(2));
        assertReflectionEquals(USER_AUTHORITIES, authorities);
        assertThat(USER_PASSWORD, is(actual.getCredentials()));
        assertThat(USER_NAME, is(actual.getName()));
    }

    @Test
    public void shouldReturnNullWhenNoAuthenticationInRequest() {
        MockHttpServletRequest request = new MockHttpServletRequest();

        Authentication actual = TokenAuthenticationService.getAuthentication(request);
        assertNull(actual);
    }
}
