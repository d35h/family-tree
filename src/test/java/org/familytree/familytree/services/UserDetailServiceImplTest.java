package org.familytree.familytree.services;

import org.familytree.familytree.entities.Role;
import org.familytree.familytree.entities.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserDetailServiceImplTest {

    @Mock
    UserServiceImpl userService;

    @InjectMocks
    UserDetailServiceImpl userDetailService;

    private final static String USER_NAME = "USER_NAME";

    private final static User USER = new User.Builder()
            .userEmail("email")
            .userId(1)
            .userPassword("password")
            .userName("name")
            .userRoles(asList(new Role(1, "User"), new Role(2, "Admin")))
            .build();


    @Test(expected = UsernameNotFoundException.class)
    public void shouldThrowUsernameNotFoundException() {
        when(userService.getUserByName(USER_NAME)).thenReturn(null);

        userDetailService.loadUserByUsername(USER_NAME);
    }

    @Test
    public void shouldReturnCorrectUserDetails() {
        when(userService.getUserByName(USER_NAME)).thenReturn(USER);
        UserDetails result = userDetailService.loadUserByUsername(USER_NAME);

        verify(userService).getUserByName(USER_NAME);
        verifyZeroInteractions(userService);

        assertThat(result.getAuthorities(), hasSize(2));
        assertTrue(result.getAuthorities().contains(new SimpleGrantedAuthority("User")));
        assertTrue(result.getAuthorities().contains(new SimpleGrantedAuthority("Admin")));
        assertThat(result.getName(), is(USER.getUserName()));
        assertThat(result.getPassword(), is(USER.getUserPassword()));
    }

}
