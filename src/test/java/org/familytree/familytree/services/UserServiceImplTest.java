package org.familytree.familytree.services;

import org.familytree.familytree.entities.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserServiceImpl userService;

    private final static long USER_ID = 1;
    private final static String USER_NAME = "USER_NAME";
    private final static User USER = new User.Builder()
            .userEmail("email")
            .userId(1)
            .userPassword("password")
            .userName("name")
            .build();

    @Before
    public void setUp() {
        when(userRepository.findByUserId(USER_ID)).thenReturn(USER);
        when(userRepository.findByUserName(USER_NAME)).thenReturn(USER);
        when(userRepository.save(USER)).thenReturn(USER);
    }

    @Test
    public void shouldCallFindUserById() {
        assertThat(userService.getUserById(USER_ID), is(USER));

        verify(userRepository).findByUserId(USER_ID);
        verifyNoMoreInteractions(userRepository);
    }

    @Test
    public void shouldCallFindUserByName() {
        assertThat(userService.getUserByName(USER_NAME), is(USER));

        verify(userRepository).findByUserName(USER_NAME);
        verifyNoMoreInteractions(userRepository);
    }

    @Test
    public void shouldSaveUser() {
        assertThat(userService.saveUser(USER), is(USER.getUserId()));

        verify(userRepository).save(USER);
        verifyNoMoreInteractions(userRepository);
    }

    @Test
    public void shouldReturnNull() {
        assertNull(userService.saveUser(null));

        verifyZeroInteractions(userRepository);
    }

}
