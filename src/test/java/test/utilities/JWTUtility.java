package test.utilities;

import static java.util.Arrays.*;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * JWT utility, this class is used to facilitate testing of JWT functionality.
 */
public class JWTUtility {

    public static final String CLAIMS_AUTHORITIES_KEY = "authorities";

    public static final String CLAIMS_CREDENTIALS_KEY = "credentials";

    public static final String USER_NAME = "USER_NAME";

    public static final String USER_PASSWORD = "USER_PASSWORD";

    public static final String CLAIMS_SEPARATOR = ",";

    public static final long EXPIRATION_TIME = 864_000_000; // 10 days

    public static final String SECRET = "ThisIsASecret";

    public static final long CURRENT_MILLI_SECONDS = 1;

    public static final String HEADER_AUTHORIZATION = "Authorization";

    public static final List<SimpleGrantedAuthority> USER_AUTHORITIES = asList(new SimpleGrantedAuthority("User"), new SimpleGrantedAuthority("Admin"));

    public static final  Authentication DEFAULT_AUTHENTICATION = getDefaultAuthentication();

    public static final String DEFAULT_JWT_STRING = createJWTString(DEFAULT_AUTHENTICATION);

    private static String createJWTString(Authentication auth) {
        Claims claims = Jwts.claims().setSubject(auth.getName());
        claims.put(CLAIMS_AUTHORITIES_KEY,
                auth.getAuthorities().stream().map(authority -> authority.getAuthority()).collect(
                        Collectors.joining(CLAIMS_SEPARATOR)));
        claims.put(CLAIMS_CREDENTIALS_KEY, auth.getCredentials());

        String JWT = Jwts.builder()
                .setClaims(claims)
                .setExpiration(new Date(CURRENT_MILLI_SECONDS + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();

        return JWT;
    }

    private static Authentication getDefaultAuthentication() {
        return new UsernamePasswordAuthenticationToken(USER_NAME, USER_PASSWORD, USER_AUTHORITIES);
    }


}
